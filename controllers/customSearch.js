var request = require("request");

module.exports = {
    getPage: function (url) {
        var promise = new Promise(function (resolve, reject) {
            request(url, function (error, response, body) {
                if (!error && response.statusCode == 200) {
                    resolve(body);
                }
            })
        })
        return promise;
    }
}