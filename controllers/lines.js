var Promise = require('bluebird');
var fs = Promise.promisifyAll(require('fs'));
var ed = require('edit-distance');


function byEditDistance(stringA, stringB) {
    var insert, remove, update;
    insert = remove = function (node) { return 1; };
    update = function (stringA, stringB) { return stringA !== stringB ? 1 : 0; };
    var lev = ed.levenshtein(stringA, stringB, insert, remove, update);
    return lev.distance <= 1;
}

function fullMatch(stringA, stringB) {
    return stringA == stringB;
}

function entryMatch(stringA, stringB) {
    var tmpReg = new RegExp(stringB);
    return tmpReg.test(stringA);
}

function getTextFromFile(fileName) {
    return fs.readFileAsync(__dirname + fileName, 'utf8')
}

function getTexts() {
    var promise = new Promise(function (resolve, reject) {
        getTextFromFile('/../data/input.txt')
            .then(function (firstFileContent) {
                getTextFromFile('/../data/patterns.txt')
                    .then(function (secondFileContent) {
                        var texts = {
                            first: formatText(firstFileContent),
                            second: formatText(secondFileContent)
                        };
                        resolve(texts)
                    })
            })
    })
    return promise;
}

function formatText(text) {
    return text.split('\n').map(function (val) { return val.replace('\r', '') });
}

var methods = {
    1: fullMatch,
    2: entryMatch,
    3: byEditDistance
}

module.exports = {
    searchLine(method) {
        return getTexts().then(function (data) {
            var out = [];
            for (var i of data.first) {
                for (var j of data.second) {
                    if (methods[method](i, j)) {
                        out.push(i);
                        break;
                    }
                }
            }
            return new Promise(function (resolve, reject) {
                resolve(out);
            });
        });
    }
}