An aptitude test
---

What technologies I used:
---
1. Angular.js v1.6.2
2. Express.js
3. Bootstrap
4. Jquery
5. angular-ui-router
6. bluebird
7. edit-distance

---
 Angular.js for client-logic, express on backend. Bootstrap for easy buttons and jquery for selectors,
 angular-ui-router for routing on client, bluebird for ```promisifyAll()``` and edit-distance for 
 calculate edit distance. 
---
### First task
#### About google search API
I know about google search API, and at first I wanted use it, but it has limit to number of requests, 
only 100 request per day. So, I write dirty-hack: I make request to google.com with question and then
I parse responce. 
#### All work doesn't produced on client
Because of cross origin blocking. We make request to our server, from 
server we make request to google.com and if all right we send responce to client.
### Second task
I found library edit-distance, that calculate edit-distance, also I use bluebird, because it turn asynchronous
functions too easy to read.
---
### How to install/use
You should execute command:```npm launch``` in terminal from project directory,
that will install all dependencies and run app. 
If all dependencies are installed, just run ```npm start```
Then you go to http://localhost:1337 
There will be two buttons, each button for each task. 