/**
 * Write an application that prompts the user for a text string, performs a Web search
 * (Google/Yahoo/Bing/etc - your choice) and returns the title and URL of the first 
 * result. Use any tools /libraries you wish, but you must provide instructions on how to 
 * build and run your application. Please include a brief description of your application and
 * why you implemented it the way you did.
 */

app.controller('SearchController', ['$scope', 'searchService', function ($scope, searchService) {
    $scope.links = [
        {
            text: "DONT CLICK! IM TEST!",
            href: "#"
        }
    ];

    $scope.addLink = function () {
        var req = $('#linkInput').val();

        searchService.findLink(req).then(function (link) {
            // I will never do like this. I promise.
            link.href = link.href.replace('/url?q=', ''); 
            link.href = link.href.split("&")[0];

            $scope.links.push(link);
            $scope.$apply();
        });
    }
}])