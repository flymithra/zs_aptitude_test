app.service('searchService', ['$http', 'settingsService', function ($http, settingsService) {

    /* 
        Here should be google API for example, 
        but google custom search (free edition) limited to 100 requests
        for one day. 
    */

    var engine = "";
    var enginesURLS = {
            'google.com': 'https://www.google.com/search?q='
        };
    
    
    this.setEngine = (engine) => {
        this.engine = engine;
    }

    /**
     * Returns search engine, by default google.com
     */
    this.getEngine = () => {
        return this.engine || "google.com";
    }

    this.findLink = (question) => {
        var searchUrl = enginesURLS[this.getEngine()] + question;

        var findData = function (resolve, reject) {
            var options = {
                searchUrl: searchUrl
            }
            $http.post(settingsService.BASE_URL + '/search', options).then(function (data) {
                var page = data.data,
                    elem = $($(page).find('.g > .r > a')[0]);  // oh my god
                resolve({
                    text: elem.text(),
                    href: elem.attr('href')
                });
            });
        };

        return new Promise(findData);
    }

}])