var app = angular.module('app', ['ui.router']);

app.config(function ($stateProvider) {
    var stringToUrlState = {
        name: 'customSearch',
        url: '/customSearch',
        templateUrl: 'src/views/customSearch.html',
        controller: 'SearchController'
    }

    var searchLine = {
        name: 'searchLine',
        url: '/searchLine',
        templateUrl: 'src/views/strings.html',
        controller: 'StringsController'
    }

    $stateProvider.state(stringToUrlState);
    $stateProvider.state(searchLine);
})