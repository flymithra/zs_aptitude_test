app.controller('LinksController', ['$scope', 'linksService', function ($scope, linksService) {

    $scope.links = [
        {
            text: "DONT CLICK! IM TEST!",
            href: "#"
        }
    ];



    $scope.addLink = function () {
        var req = $('#linkInput').val();

        linksService.getLink(req).then(function (link) {
            $scope.links.push(link);
            $scope.$apply();
        });
    }


}])