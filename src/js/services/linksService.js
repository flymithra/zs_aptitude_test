app.service('linksService', ['$http', function ($http) {

    var searchEngine = "",
        searchEnginesURLS = {
            'google.com': 'https://www.google.com/search?q='
        };

    this.setSearchEngine = (searchEngine) => {
        this.searchEngine = searchEngine;
    }

    this.getSearchEngine = () => {
        return this.searchEngine || "google.com";
    }

    this.getLink = (url) => {
        var searchUrl = searchEnginesURLS[this.getSearchEngine()] + url;

        var findData = function (resolve, reject) {
            $http.get(searchUrl).then(function (data) {
                var page = data.data,
                    // oh my god
                    elem = $($(page).find('.g > .rc > .r > a')[0]);

                resolve({
                        text: elem.text(),
                        href: elem.attr('href')
                    });
            });
        };

        return new Promise(findData);
    }
    
}])