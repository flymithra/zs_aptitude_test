var lines = require('./controllers/lines');
var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var customSearch = require("./controllers/customSearch");

app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.post('/getLines', function (req, res) {
  lines.searchLine(req.body.method)
    .then(function (data) {
      res.send(data);
    })
})

app.listen(1337, function () {
  console.log('Example app listening on port 1337!')
})

app.post('/search', function (req, res) {
  customSearch.getPage(req.body.searchUrl)
    .then(function (data) {
      res.send(data);
    })
})
